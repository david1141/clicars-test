// Vars Definition

def GIT_REPO_CLONE_URL = '<gitlab_repository>'
def GIT_REPO_BRANCH = 'master'
def GIT_WEBHOOK_TOKEN = '<webhook token>'

def REGISTRY_USER = '<registry_user>'

def APPLICATION_YML = 'clicars-app.yml'

def APPLICATION_NAME = 'clicars-test'
def APPLICATION_IMAGE = 'clicars-test'
def APPLICATION_VERSION = '1.0.0'
def DOCKERFILE_PATH     = '.'

def HELM_CHART = 'clicars-app/'



properties([
// Gitlab Webhook
    gitLabConnection('gitlab-connection-name'),
    pipelineTriggers([
        [
            $class: 'GitLabPushTrigger',
            branchFilterType: 'All',
            triggerOnPush: true,
            secretToken: GIT_WEBHOOK_TOKEN,
        ]
    ])
])

// Pipeline
pipeline {
    agent any
    stages {
        stage ('Clone repository') {
            steps {
               checkout([
                    $class: 'GitSCM',
                    branches: [[name: "${GIT_REPO_BRANCH}"]],
                    userRemoteConfigs: [[
                        url: "${GIT_REPO_CLONE_URL}",
                        credentialsId: 'jenkins-gitlab-credentials'
                    ]],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [],
                    gitTool: 'Default',
                    submoduleCfg: []
                ])
            }
        }
        stage('Build application') {
            steps {

// Lets assume we have created a string credential in jenkins with the docker registry password

              withCredentials([string(credentialsId: 'jenkins-registry-pass', variable: 'REGISTRY_PASS')]) {
              sh """

                docker build -t ${APPLICATION_IMAGE}:${APPLICATION_VERSION} ${DOCKERFILE_PATH}
                docker login --username ${REGISTRY_USER} --password ${REGISTRY_PASS}
                docker push ${APPLICATION_IMAGE}:${APPLICATION_VERSION}

              """
              }
            }
        }
        stage('Deploy Application') {
            steps {

// Lets assume we have created a file credential in jenkins with the .kube/config file

              withCredentials([file(credentialsId: 'jenkins-kubeconfig-file', variable: 'KUBECONFIG')]) {
              sh """

                sed -i "s,image:.*${APPLICATION_IMAGE}.*,image: ${APPLICATION_IMAGE}:${APPLICATION_VERSION},g" ${APPLICATION_YML}
                kubectl apply -f ${APPLICATION_YML}

              """
              }
            }
        }
    }
}

