#!/usr/bin/python3

import requests
import re

# Request to elasticsearch cluster to get its data
elastic_req = requests.get('http://localhost:9201/_nodes?pretty').json()

# Get and parse data in each elasticsearch node ID
for i in elastic_req["nodes"]:
  e_node_name = elastic_req["nodes"][i]["name"]
  e_node_addr = elastic_req["nodes"][i]["http_address"] 
  e_node_data = re.findall('inet\[\/(.+?)\]', e_node_addr)[0].split(":")

  print(e_node_name + " " + e_node_data[1] + " " + e_node_data[0])

