# SRE Clicars test

Project created to upload the different task for the clicars SRE vacancy technical test.

## Structure
The project structure is defined in folders, each one with the different tasks to accomplish the test.

- infra : Created the initial infrastructure in localhost with microk8s.
- docker_and_kubernetes: Split in 2 folders, creates the Docker image and Deploys the service in Kubernetes. It has an small script to test the application site.
- jenkins: Contains a groovy pipeline which builds an deploys an application after receiving a push webhook from a repository.
- sql_query: Has two sql files. One to populate the database with the needed data, and another with the asked query.
- python_program: Last, theres a python script for the "Query api and parse json using python" task.


