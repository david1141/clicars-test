#!/bin/bash

## Update package repository
sudo apt update

## Install Docker
sudo apt install -y docker.io

## Install K8s
sudo snap install kubectl --classic
sudo snap install microk8s --classic
microk8s start

mkdir -p $HOME/.kube
microk8s config > $HOME/.kube/config
microk8s enable dns
microk8s enable ingress
microk8s enable storage

## Install Jenkins
sudo apt install -y openjdk-11-jdk wget
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update
sudo apt install -y jenkins
